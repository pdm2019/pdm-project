-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `author` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `aname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `bod` date DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'Harper Lee','1999-05-14'),(2,'Jane Austen','1999-05-14'),(3,'Anne Frank','1999-05-14'),(4,'George Orwell','1999-05-14'),(5,'J.K. Rowling','1999-05-14'),(6,'J.R.R. Tolkien','1999-05-14'),(7,'F. Scott Fitzgerald','1999-05-14'),(8,'E.B. White','1999-05-14'),(9,'Louisa May Alcott','1999-05-14'),(10,'Ray Bradbury','1999-05-14'),(11,'Charlotte Bronte','1999-05-14'),(12,'Margaret Mitchell','1999-05-14'),(13,'J.D. Salinger','1999-05-14'),(14,'Markus Zusak','1999-05-14'),(15,'Mark Twain','1999-05-14'),(16,'Suzanne Collins','1999-05-14'),(17,'Kathryn Stockett','1999-05-14'),(18,'C.S. Lewis','1999-05-14'),(19,'John Steinbeck','1999-05-14'),(20,'William Golding','1999-05-14'),(23,'Test 4','1999-05-14');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `book` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `bname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `bnum` int(11) DEFAULT NULL,
  `timeprint` int(11) DEFAULT NULL,
  `ymanufacture` int(11) DEFAULT NULL,
  `manufacturer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `aid` int(11) DEFAULT NULL,
  `cateid` int(11) DEFAULT NULL,
  `isborrowed` int(11) DEFAULT NULL,
  PRIMARY KEY (`bid`),
  KEY `cateid` (`cateid`),
  KEY `aid` (`aid`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`cateid`) REFERENCES `category` (`cateid`),
  CONSTRAINT `book_ibfk_2` FOREIGN KEY (`aid`) REFERENCES `author` (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Divine Songs',10,102,1715,'Hành Chính',6,5,9),(2,'A Description of Three Hundred Animals',10,10,1730,'Kim Đồng',1,2,4),(3,'The Gigantick History of the Two Famous Giants',10,10,1730,'Kim Đồng',8,1,4),(4,'A Little Pretty Pocket-Book ',10,10,1744,'Kim Đồng',1,2,5),(7,'Tết Sum Vầy',12,12,2019,'Nguyễn Lâm Thành',3,19,0),(8,'Test 2',12,12,2018,'Tâm Thần',19,19,0);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardborrow`
--

DROP TABLE IF EXISTS `cardborrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cardborrow` (
  `reid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  PRIMARY KEY (`reid`,`bid`),
  KEY `bid` (`bid`),
  CONSTRAINT `cardborrow_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `book` (`bid`),
  CONSTRAINT `cardborrow_ibfk_2` FOREIGN KEY (`reid`) REFERENCES `record` (`reid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardborrow`
--

LOCK TABLES `cardborrow` WRITE;
/*!40000 ALTER TABLE `cardborrow` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardborrow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `cateid` int(11) NOT NULL AUTO_INCREMENT,
  `catename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `catenum` int(11) DEFAULT NULL,
  PRIMARY KEY (`cateid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Action and adventure',10),(2,'Alternate history',10),(3,'Anthology',10),(4,'Chick lit',10),(5,'Children\'s',10),(6,'Comic book',10),(7,'Coming-of-age',10),(8,'Crime',10),(9,'Drama',10),(10,'Fairytale',10),(11,'Fantasy',10),(12,'Graphic novel',10),(13,'Historical fiction',10),(14,'Horror',10),(15,'Mystery',10),(16,'Paranormal romance',10),(17,'Picture book',10),(18,'Political thriller',10),(19,'Romance',10),(20,'Satire',10),(21,'Test2',10);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `department` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `dname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Computer Science and Engineering'),(3,'Test1'),(4,'Test3'),(5,'Test4'),(6,'Test6'),(7,'Test7'),(8,'Test8'),(9,'Test9'),(10,'Test5');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record`
--

DROP TABLE IF EXISTS `record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `record` (
  `reid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `stid` int(11) DEFAULT NULL,
  `borrowdate` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  PRIMARY KEY (`reid`),
  KEY `sid` (`sid`),
  KEY `stid` (`stid`),
  CONSTRAINT `record_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `student` (`sid`),
  CONSTRAINT `record_ibfk_2` FOREIGN KEY (`stid`) REFERENCES `staff` (`stid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record`
--

LOCK TABLES `record` WRITE;
/*!40000 ALTER TABLE `record` DISABLE KEYS */;
INSERT INTO `record` VALUES (7,1,1,'2019-05-12','2019-05-19'),(13,2,4,'2019-05-12','2019-05-19'),(14,1,1,'2019-05-12','2019-05-19'),(15,1,1,'2019-05-12','2019-05-19'),(16,1,1,'2019-05-12','2019-05-19');
/*!40000 ALTER TABLE `record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `staff` (
  `stid` int(11) NOT NULL AUTO_INCREMENT,
  `stname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `bod` date DEFAULT NULL,
  PRIMARY KEY (`stid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'Nguyen Lam Thanh','1999-05-14'),(2,'Nguyen Lam Thanh 1','1999-05-14'),(3,'Nguyen Lam Thanh 2','1999-05-14'),(4,'Nguyen Lam Thanh 3','1999-05-14'),(5,'Nguyen Lam Thanh 4','1999-05-14'),(6,'Nguyen Lam Thanh 5','1999-05-14'),(8,'Nguyen Lam Thanh 9','1999-05-14');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `fname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `lname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `mname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `bod` date DEFAULT NULL,
  PRIMARY KEY (`sid`),
  KEY `did` (`did`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`did`) REFERENCES `department` (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,1,'Nguyen','Lam','Thanh','1999-05-14'),(2,6,'Nguyen','Lam','Thanh 2','1999-05-14'),(3,4,'Nguyen','Lam','Thanh 3','1999-05-14');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'library'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-12 21:28:03
