package pdm.library.swing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.TabableView;

import pdm.library.entities.Author;
import pdm.library.entities.Book;
import pdm.library.entities.Category;
import pdm.library.entities.Database;
import pdm.library.entities.Department;
import pdm.library.entities.Record;
import pdm.library.entities.RegexClass;
import pdm.library.entities.Staff;
import pdm.library.entities.Student;
import pdm.library.myconnect.MyConnect;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class Swing extends JFrame {
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtBookRe;
	private JTextField txtBookYear;
	private JTextField txtBookMan;
	private JTextField txtBookNum;
	private JTextField txtBookName;
	private JTable tableBook;

	private Connection cn;
	private PreparedStatement ps;
	private ResultSet rs;
	private ResultSetMetaData remeta;
	private JTextField txtCateNum;
	private JTextField txtCateName;
	private JTable tableCate;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	private int t = -1;
	private boolean isFirstClick = false;
	private int m = -1;
	private boolean isFirstClickBook = false;
	int rowBook1 = -1;
	int rowBook2 = -1;

	private boolean isFirstClickDe = false;
	int rowDe1 = -1;
	int rowDe2 = -1;

	private boolean isFirstClickStu = false;
	int rowStu1 = -1;
	int rowStu2 = -1;

	private boolean isFirstClickAu = false;
	int rowAu1 = -1;
	int rowAu2 = -1;

	private boolean isFirstClickSta = false;
	int rowSta1 = -1;
	int rowSta2 = -1;

	private int isSelected = -1;

	private JTable tableBorrowBook;
	private JTable tableStudent;
	private JTextField txtStudentFname;
	private JTextField txtStudentLname;
	private JTextField txtStudentDate;
	private JTextField txtStudentMname;
	private JTable tableDepartment;
	private JTextField txtDeName;
	private JTable tableStaff;
	private JTextField txtStaffName;
	private JTextField txtStaffDate;
	private JTable tableRecord;
	private JTable tableAuthor;
	private JTextField txtAuthorName;
	private JTextField txtAuthorDate;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private final ButtonGroup buttonGroup_4 = new ButtonGroup();
	private final ButtonGroup buttonGroup_5 = new ButtonGroup();

	private List<Integer> listSid;
	private List<Integer> listStid;

	public Swing() {
		getContentPane().setLayout(null);
		Category category = new Category();
		Author author = new Author();
		Book book = new Book();
		Department department = new Department();
		Student student = new Student();
		Staff staff = new Staff();
		Record record = new Record();
		listSid = student.getIds();
		listStid = staff.getIds();

		JLabel lblTitle = new JLabel("Library Management");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(354, 11, 352, 48);
		lblTitle.setHorizontalTextPosition(JLabel.CENTER);
		getContentPane().add(lblTitle);

		JPanel pnContainer = new JPanel();
		pnContainer.setBounds(10, 55, 1079, 688);
		getContentPane().add(pnContainer);
		pnContainer.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 1059, 666);
		pnContainer.add(tabbedPane);

		JPanel pnCate = new JPanel();
		tabbedPane.addTab("Category", null, pnCate, null);
		pnCate.setLayout(null);

		JLabel lblCate = new JLabel("Category Management");
		lblCate.setHorizontalTextPosition(JLabel.CENTER);
		lblCate.setHorizontalAlignment(SwingConstants.CENTER);
		lblCate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCate.setBounds(361, 11, 305, 42);
		pnCate.add(lblCate);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(10, 566, 1034, 61);
		pnCate.add(panel_1);

		JButton btnCateInsert = new JButton("Insert");
		btnCateInsert.setEnabled(false);
		btnCateInsert.setBounds(147, 5, 148, 45);
		panel_1.add(btnCateInsert);

		JButton btnCateUpdate = new JButton("Update");
		btnCateUpdate.setEnabled(false);
		btnCateUpdate.setBounds(442, 5, 148, 45);
		panel_1.add(btnCateUpdate);

		JButton btnCateDelete = new JButton("Delete");
		btnCateDelete.setEnabled(false);
		btnCateDelete.setBounds(737, 5, 148, 45);
		panel_1.add(btnCateDelete);

		JLabel lblCateQuery = new JLabel("Query");
		lblCateQuery.setHorizontalAlignment(SwingConstants.CENTER);
		lblCateQuery.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCateQuery.setBounds(10, 54, 297, 33);
		pnCate.add(lblCateQuery);

		JButton btnCateRun = new JButton("Run");
		btnCateRun.setEnabled(true);
		btnCateRun.setBounds(115, 219, 89, 23);
		pnCate.add(btnCateRun);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 260, 1034, 42);
		pnCate.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdCateQuery = new JRadioButton("Query");
		buttonGroup_1.add(rdCateQuery);
		rdCateQuery.setSelected(true);
		panel_2.add(rdCateQuery);

		JRadioButton rdCateButton = new JRadioButton("Buton");
		buttonGroup_1.add(rdCateButton);
		panel_2.add(rdCateButton);

		txtCateNum = new JTextField();
		txtCateNum.setEnabled(false);
		txtCateNum.setColumns(10);
		txtCateNum.setBounds(115, 377, 192, 20);
		pnCate.add(txtCateNum);

		txtCateName = new JTextField();
		txtCateName.setEnabled(false);
		txtCateName.setColumns(10);
		txtCateName.setBounds(115, 346, 192, 20);
		pnCate.add(txtCateName);

		JLabel lblCateName = new JLabel("Category name");
		lblCateName.setBounds(10, 349, 95, 14);
		pnCate.add(lblCateName);

		JLabel lblCateQuantity = new JLabel("Quantity");
		lblCateQuantity.setBounds(10, 380, 95, 14);
		pnCate.add(lblCateQuantity);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 100, 295, 108);
		pnCate.add(scrollPane_3);

		JTextArea txtCateQuey = new JTextArea();
		scrollPane_3.setViewportView(txtCateQuey);
		txtCateQuey.setLineWrap(true);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(328, 101, 714, 108);
		pnCate.add(scrollPane_4);

		JTextArea txtCateResult = new JTextArea();
		scrollPane_4.setViewportView(txtCateResult);
		txtCateResult.setLineWrap(true);

		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(328, 346, 716, 210);
		pnCate.add(scrollPane_5);

		DefaultTableModel modelCate = new DefaultTableModel();
		Object[] row = new Object[3];
		tableCate = new JTable();
		modelCate.setColumnIdentifiers(category.getTitles());

		for (Category c : category.getAll()) {
			row[0] = c.getId();
			row[1] = c.getName();
			row[2] = c.getNum();
			modelCate.addRow(row);
			tableCate.setModel(modelCate);
		}
		scrollPane_5.setViewportView(tableCate);

		JPanel pnBorrow = new JPanel();
		tabbedPane.addTab("Borrow", null, pnBorrow, null);
		pnBorrow.setLayout(null);

		JLabel lblBorrow = new JLabel("Borrow");
		lblBorrow.setHorizontalTextPosition(JLabel.CENTER);
		lblBorrow.setHorizontalAlignment(SwingConstants.CENTER);
		lblBorrow.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBorrow.setBounds(338, 11, 305, 42);
		pnBorrow.add(lblBorrow);

		JLabel lblBorrowQuery = new JLabel("Query");
		lblBorrowQuery.setHorizontalAlignment(SwingConstants.CENTER);
		lblBorrowQuery.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBorrowQuery.setBounds(10, 47, 297, 33);
		pnBorrow.add(lblBorrowQuery);

		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(10, 91, 293, 106);
		pnBorrow.add(scrollPane_6);

		JTextArea txtBorrowQuery = new JTextArea();
		scrollPane_6.setViewportView(txtBorrowQuery);
		txtBorrowQuery.setLineWrap(true);

		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setBounds(332, 92, 712, 106);
		pnBorrow.add(scrollPane_7);

		JTextArea txtBorrowResult = new JTextArea();
		scrollPane_7.setViewportView(txtBorrowResult);
		txtBorrowResult.setLineWrap(true);

		JScrollPane scrollPane_9 = new JScrollPane();
		scrollPane_9.setBounds(500, 243, 544, 340);
		pnBorrow.add(scrollPane_9);

		tableBorrowBook = new JTable();
		DefaultTableModel modelBorrow = new DefaultTableModel();
		Object[] rowBorrow = new Object[9];
		modelBorrow.setColumnIdentifiers(book.getTitles());
		tableBorrowBook.setModel(modelBorrow);

		for (Book b : book.getAll()) {
			rowBorrow[0] = b.getId();
			rowBorrow[1] = b.getName();
			rowBorrow[2] = b.getNum();
			rowBorrow[3] = b.getRepublish();
			rowBorrow[4] = b.getYear();
			rowBorrow[5] = b.getManufacturer();
			rowBorrow[6] = b.getAid();
			rowBorrow[7] = b.getCateid();
			rowBorrow[8] = b.getBorrowed();
			modelBorrow.addRow(rowBorrow);
			tableBorrowBook.setModel(modelBorrow);
		}
		scrollPane_9.setViewportView(tableBorrowBook);

		JButton btnBorrow = new JButton("Borrow");
		btnBorrow.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnBorrow.setBounds(202, 594, 170, 33);
		pnBorrow.add(btnBorrow);

		JComboBox<String> cbBorrowStaff = new JComboBox<String>();
		cbBorrowStaff.setBounds(476, 594, 170, 33);
		for (String s : staff.getNames())
			cbBorrowStaff.addItem(s);
		pnBorrow.add(cbBorrowStaff);

		JComboBox<String> cbBorrowStudent = new JComboBox<String>();
		cbBorrowStudent.setBounds(744, 594, 170, 33);
		for (String s : student.getNames())
			cbBorrowStudent.addItem(s);
		pnBorrow.add(cbBorrowStudent);

		JScrollPane scrollPane_8 = new JScrollPane();
		scrollPane_8.setBounds(10, 243, 480, 149);
		pnBorrow.add(scrollPane_8);

		tableRecord = new JTable();
		DefaultTableModel modelRecord = new DefaultTableModel();
		Object[] rowRecord = new Object[5];
		modelRecord.setColumnIdentifiers(record.getTitles());
		tableRecord.setModel(modelRecord);

		for (Record r : record.getAll()) {
			rowRecord[0] = r.getId();
			rowRecord[1] = r.getSid();
			rowRecord[2] = r.getStid();
			rowRecord[3] = r.getBorrowdate();
			rowRecord[4] = r.getDuedate();
			modelRecord.addRow(rowRecord);
			tableRecord.setModel(modelRecord);
		}
		scrollPane_8.setViewportView(tableRecord);

		JTextArea txtBorrowBook = new JTextArea();
		txtBorrowBook.setBounds(10, 462, 480, 121);
		txtBorrowBook.setEditable(false);
		pnBorrow.add(txtBorrowBook);

		JButton btnReturn = new JButton("Return");
		btnReturn.setBounds(214, 405, 89, 23);
		pnBorrow.add(btnReturn);

		JButton btnBorrowRun = new JButton("Run");
		btnBorrowRun.setBounds(214, 208, 89, 23);
		pnBorrow.add(btnBorrowRun);

		JButton btnView = new JButton("View");
		btnView.setBounds(10, 208, 89, 23);
		pnBorrow.add(btnView);

		JButton btnRemove = new JButton("Remove");
		btnRemove.setBounds(10, 428, 89, 23);
		pnBorrow.add(btnRemove);

		JPanel pnBook = new JPanel();
		tabbedPane.addTab("Book", null, pnBook, null);
		pnBook.setLayout(null);

		JLabel lblBook = new JLabel("Book Management");
		lblBook.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBook.setHorizontalAlignment(SwingConstants.CENTER);
		lblBook.setBounds(363, 11, 305, 42);
		lblBook.setHorizontalTextPosition(SwingConstants.CENTER);
		pnBook.add(lblBook);

		JPanel panel = new JPanel();
		panel.setBounds(10, 566, 1034, 61);
		pnBook.add(panel);
		panel.setLayout(null);

		JButton btnBookInsert = new JButton("Insert");
		btnBookInsert.setBounds(147, 5, 148, 45);
		panel.add(btnBookInsert);

		JButton btnBookUpdate = new JButton("Update");
		btnBookUpdate.setBounds(442, 5, 148, 45);
		panel.add(btnBookUpdate);

		JButton btnBookDelete = new JButton("Delete");
		btnBookDelete.setBounds(737, 5, 148, 45);
		panel.add(btnBookDelete);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 98, 297, 110);
		pnBook.add(scrollPane_1);

		JTextArea txtQuery = new JTextArea();
		scrollPane_1.setViewportView(txtQuery);
		txtQuery.setLineWrap(true);

		JLabel lblQuery = new JLabel("Query");
		lblQuery.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblQuery.setHorizontalAlignment(SwingConstants.CENTER);
		lblQuery.setBounds(10, 54, 297, 33);
		pnBook.add(lblQuery);

		JButton btnRun = new JButton("Run");
		btnRun.setBounds(115, 219, 89, 23);
		pnBook.add(btnRun);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(328, 98, 716, 110);
		pnBook.add(scrollPane);

		JTextArea txtQueryResult = new JTextArea();
		txtQueryResult.setLineWrap(true);
		scrollPane.setViewportView(txtQueryResult);

		JPanel pnRd = new JPanel();
		pnRd.setBounds(10, 260, 1034, 42);
		pnBook.add(pnRd);
		pnRd.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdQuery = new JRadioButton("Query");
		buttonGroup.add(rdQuery);
		pnRd.add(rdQuery);

		JRadioButton rdButton = new JRadioButton("Buton");
		buttonGroup.add(rdButton);
		pnRd.add(rdButton);

		txtBookRe = new JTextField();
		txtBookRe.setColumns(10);
		txtBookRe.setBounds(115, 408, 192, 20);
		pnBook.add(txtBookRe);

		txtBookYear = new JTextField();
		txtBookYear.setColumns(10);
		txtBookYear.setBounds(115, 439, 192, 20);
		pnBook.add(txtBookYear);

		txtBookMan = new JTextField();
		txtBookMan.setColumns(10);
		txtBookMan.setBounds(115, 473, 192, 20);
		pnBook.add(txtBookMan);

		txtBookNum = new JTextField();
		txtBookNum.setColumns(10);
		txtBookNum.setBounds(115, 504, 192, 20);
		pnBook.add(txtBookNum);

		String cateName[] = new String[category.getAllCateName().size()];
		for (String s : category.getAllCateName())
			cateName[category.getAllCateName().indexOf(s)] = s;

		JComboBox<String> cbBookCate = new JComboBox<String>();
		cbBookCate.setBounds(115, 535, 192, 20);
		for (String s : category.getAllCateName())
			cbBookCate.addItem(s);
		pnBook.add(cbBookCate);

		txtBookName = new JTextField();
		txtBookName.setColumns(10);
		txtBookName.setBounds(115, 346, 192, 20);
		pnBook.add(txtBookName);

		JLabel lblBookName = new JLabel("Book name");
		lblBookName.setBounds(10, 349, 95, 14);
		pnBook.add(lblBookName);

		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 380, 95, 14);
		pnBook.add(lblAuthor);

		JLabel lblReprint = new JLabel("Republish");
		lblReprint.setBounds(10, 411, 95, 14);
		pnBook.add(lblReprint);

		JLabel lblYManu = new JLabel("Year manuafacture");
		lblYManu.setBounds(10, 442, 95, 14);
		pnBook.add(lblYManu);

		JLabel lblManufac = new JLabel("Manufacturer");
		lblManufac.setBounds(10, 476, 95, 14);
		pnBook.add(lblManufac);

		JLabel lblBookNum = new JLabel("Quantity");
		lblBookNum.setBounds(10, 507, 95, 14);
		pnBook.add(lblBookNum);

		JLabel lblCbCate = new JLabel("Category");
		lblCbCate.setBounds(10, 541, 95, 14);
		pnBook.add(lblCbCate);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(328, 346, 716, 209);
		pnBook.add(scrollPane_2);

		DefaultTableModel modelBook = new DefaultTableModel();
		Object[] rowBook = new Object[book.getNumField()];
		modelBook.setColumnIdentifiers(book.getTitles());
		tableBook = new JTable();

		for (Book b : book.getAll()) {
			rowBook[0] = b.getId();
			rowBook[1] = b.getName();
			rowBook[2] = b.getNum();
			rowBook[3] = b.getRepublish();
			rowBook[4] = b.getYear();
			rowBook[5] = b.getManufacturer();
			rowBook[6] = b.getAid();
			rowBook[7] = b.getCateid();
			rowBook[8] = b.getBorrowed();
			modelBook.addRow(rowBook);
			tableBook.setModel(modelBook);
		}

		scrollPane_2.setViewportView(tableBook);

		btnRun.setEnabled(true);

		JComboBox<String> cbAuthor = new JComboBox<String>();
		cbAuthor.setBounds(115, 377, 192, 20);
		for (String s : author.getNames())
			cbAuthor.addItem(s);
		pnBook.add(cbAuthor);
		//

		// Event
		cbBookCate.addActionListener(event -> {
			System.out.println(cbBookCate.getSelectedItem());
			System.out.println(cbBookCate.getSelectedIndex());
		});

		// Radio button
		rdQuery.addActionListener(event -> {
			btnBookDelete.setEnabled(false);
			btnBookInsert.setEnabled(false);
			btnBookUpdate.setEnabled(false);
			txtBookName.setEnabled(false);
			txtBookRe.setEnabled(false);
			txtBookYear.setEnabled(false);
			txtBookMan.setEnabled(false);
			txtBookNum.setEnabled(false);
			cbBookCate.setEnabled(false);
			cbAuthor.setEnabled(false);

			btnRun.setEnabled(true);
		});

		rdButton.addActionListener(event -> {
			btnBookDelete.setEnabled(true);
			btnBookInsert.setEnabled(true);
			btnBookUpdate.setEnabled(true);
			txtBookName.setEnabled(true);
			txtBookRe.setEnabled(true);
			txtBookYear.setEnabled(true);
			txtBookMan.setEnabled(true);
			txtBookNum.setEnabled(true);
			cbBookCate.setEnabled(true);
			cbAuthor.setEnabled(true);

			btnRun.setEnabled(false);
		});

		btnRun.addActionListener(event -> {

			if (!RegexClass.isEmpty(txtQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtQuery.getText());
					rs = ps.executeQuery();
					ResultSetMetaData meta = rs.getMetaData();
					for (int i = 1; i < meta.getColumnCount() + 1; i++)
						s += (meta.getColumnName(i).toUpperCase() + "\t\t");
					s += "\n";
					while (rs.next()) {
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (rs.getString(i) + "\t\t");
						s += "\n";
					}
					txtQueryResult.setText(s);
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {

			}
		});

		btnBookInsert.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtBookName.getText())) {
				if (!RegexClass.isEmpty(txtBookMan.getText())) {
					if (!RegexClass.isEmpty(txtBookNum.getText())) {
						if (!RegexClass.isEmpty(txtBookRe.getText())) {
							if (!RegexClass.isEmpty(txtBookYear.getText())) {
								try {
									String sql = "insert into book(bname, bnum, timeprint, ymanufacture, manufacturer, aid, cateid, isborrowed) values (?,?,?,?,?,?,?,?)";
									cn = new MyConnect().getcn();
									ps = cn.prepareStatement(sql);
									ps.setString(1, txtBookName.getText());
									ps.setInt(2, Integer.parseInt(txtBookNum.getText()));
									ps.setInt(3, Integer.parseInt(txtBookRe.getText()));
									ps.setInt(4, Integer.parseInt(txtBookYear.getText()));
									ps.setString(5, txtBookMan.getText());
									ps.setInt(6, author.getIdByName((String) cbAuthor.getSelectedItem()));
									ps.setInt(7, category.getIdByName((String) cbBookCate.getSelectedItem()));
									ps.setInt(8, 0);

									// Display query string
									txtQueryResult.setText("Query is used : \n"
											+ ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
									//

									ps.executeUpdate();
									ps.close();
									DefaultTableModel model1 = new DefaultTableModel();
									Object[] row2 = new Object[book.getNumField()];
									model1.setColumnIdentifiers(book.getTitles());
									for (Book b : book.getAll()) {
										row2[0] = b.getId();
										row2[1] = b.getName();
										row2[2] = b.getNum();
										row2[3] = b.getRepublish();
										row2[4] = b.getYear();
										row2[5] = b.getManufacturer();
										row2[6] = b.getAid();
										row2[7] = b.getCateid();
										row2[8] = b.getBorrowed();
										model1.addRow(row2);
										tableBook.setModel(model1);
									}

								} catch (SQLException e) {
									e.printStackTrace();
								}
							} else {

							}
						} else {

						}
					} else {

					}
				} else {

				}
			} else {

			}
		});

		btnBookUpdate.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtBookName.getText())) {
				if (!RegexClass.isEmpty(txtBookMan.getText())) {
					if (!RegexClass.isEmpty(txtBookNum.getText())) {
						if (!RegexClass.isEmpty(txtBookRe.getText())) {
							if (!RegexClass.isEmpty(txtBookYear.getText())) {
								try {
									String sql = "update book set bname = ?, bnum = ?, timeprint = ?, ymanufacture = ?, manufacturer = ?, aid = ?, cateid = ? where bid = ?";
									cn = new MyConnect().getcn();
									ps = cn.prepareStatement(sql);
									ps.setString(1, txtBookName.getText());
									ps.setInt(2, Integer.parseInt(txtBookNum.getText()));
									ps.setInt(3, Integer.parseInt(txtBookRe.getText()));
									ps.setInt(4, Integer.parseInt(txtBookYear.getText()));
									ps.setString(5, txtBookMan.getText());
									ps.setInt(6, author.getIdByName((String) cbAuthor.getSelectedItem()));
									ps.setInt(7, category.getIdByName((String) cbBookCate.getSelectedItem()));
									ps.setInt(8, (int) tableBook.getValueAt(rowBook1, 0));

									// Display query string
									txtQueryResult.setText("Query is used : \n"
											+ ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
									//

									ps.executeUpdate();
									ps.close();
									DefaultTableModel model1 = new DefaultTableModel();
									Object[] row2 = new Object[book.getNumField()];
									model1.setColumnIdentifiers(book.getTitles());
									for (Book b : book.getAll()) {
										row2[0] = b.getId();
										row2[1] = b.getName();
										row2[2] = b.getNum();
										row2[3] = b.getRepublish();
										row2[4] = b.getYear();
										row2[5] = b.getManufacturer();
										row2[6] = b.getAid();
										row2[7] = b.getCateid();
										row2[8] = b.getBorrowed();
										model1.addRow(row2);
										tableBook.setModel(model1);
									}

								} catch (SQLException e) {
									e.printStackTrace();
								}
							} else {

							}
						} else {

						}
					} else {

					}
				} else {

				}
			} else {

			}
		});

		btnBookDelete.addActionListener(event -> {
			if (rowBook1 != -1) {
				if (rowBook1 != rowBook2) {
					try {
						DefaultTableModel model = new DefaultTableModel();
						String sql = "delete from book where bid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableBook.getValueAt(rowBook1, 0));

						// Display query string
						txtCateResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						model = (DefaultTableModel) tableBook.getModel();
						model.removeRow(rowBook1);
						rowBook2 = rowBook1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		tableBook.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				isFirstClickBook = true;
				rowBook1 = tableBook.getSelectedRow();
				rowBook2 = -1;
				txtBookName.setText((String) tableBook.getValueAt(rowBook1, 1));
				txtBookMan.setText(tableBook.getValueAt(rowBook1, 5) + "");
				txtBookNum.setText(tableBook.getValueAt(rowBook1, 2) + "");
				txtBookRe.setText(tableBook.getValueAt(rowBook1, 3) + "");
				txtBookYear.setText(tableBook.getValueAt(rowBook1, 4) + "");
				// Get author by id
				String sql = "select aname from author where aid = ?";
				Connection cn = Database.getConnetion();
				try {
					PreparedStatement ps = cn.prepareStatement(sql);
					ps.setInt(1, Integer.parseInt(tableBook.getValueAt(rowBook1, 6) + ""));
					ResultSet rs = ps.executeQuery();

					if (rs.next())
						cbAuthor.setSelectedItem(rs.getString(1));
					rs.close();
					ps.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//

				// Get category by id sql = "select catename from category where cateid = ?";
				cn = Database.getConnetion();
				try {
					PreparedStatement ps = cn.prepareStatement(sql);
					ps.setInt(1, Integer.parseInt(tableBook.getValueAt(rowBook1, 7) + ""));
					ResultSet rs = ps.executeQuery();

					if (rs.next())
						cbBookCate.setSelectedItem(rs.getString(1));
					rs.close();
					ps.close();
				} catch (SQLException e1) { // TODO Auto-generated catch block
					e1.printStackTrace();
				} //
			}
		});

		rdCateQuery.addActionListener(even -> {
			btnCateDelete.setEnabled(false);
			btnCateInsert.setEnabled(false);
			btnCateUpdate.setEnabled(false);
			txtCateName.setEnabled(false);
			txtCateNum.setEnabled(false);

			btnCateRun.setEnabled(true);
		});

		rdCateButton.addActionListener(even -> {
			btnCateDelete.setEnabled(true);
			btnCateInsert.setEnabled(true);
			btnCateUpdate.setEnabled(true);
			txtCateName.setEnabled(true);
			txtCateNum.setEnabled(true);

			btnCateRun.setEnabled(false);
		});

		btnCateRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtCateQuey.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtCateQuey.getText());
					rs = ps.executeQuery();
					ResultSetMetaData meta = rs.getMetaData();
					for (int i = 1; i < meta.getColumnCount() + 1; i++)
						s += (meta.getColumnName(i).toUpperCase() + "\t\t");
					s += "\n";
					while (rs.next()) {
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (rs.getString(i) + "\t\t");
						s += "\n";
					}
					txtCateResult.setText(s);
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {

			}
		});

		tableCate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t = tableCate.getSelectedRow();
				m = -1;
				txtCateName.setText((String) tableCate.getValueAt(t, 1));
				txtCateNum.setText(String.valueOf(tableCate.getValueAt(t, 2)));
				isFirstClick = true;
			}
		});

		btnCateInsert.addActionListener(event -> {
			if (!isFirstClick) {
				if (!RegexClass.isEmpty(txtCateName.getText())) {
					if (!RegexClass.isEmpty(txtCateNum.getText())) {
						try {
							DefaultTableModel model = new DefaultTableModel();
							String sql = "insert into category(catename,catenum) values(?, ?)";
							cn = new MyConnect().getcn();
							ps = cn.prepareStatement(sql);
							ps.setString(1, txtCateName.getText());
							ps.setInt(2, Integer.parseInt(txtCateNum.getText()));

							// Display query string
							txtCateResult.setText("Query is used : \n"
									+ ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
							//

							ps.executeUpdate();
							ps.close();
							model = (DefaultTableModel) tableCate.getModel();
							model.setRowCount(0);
							for (Category c : category.getAll()) {
								row[0] = c.getId();
								row[1] = c.getName();
								row[2] = c.getNum();
								model.addRow(row);
								tableCate.setModel(model);
							}
							txtCateName.setText("");
							txtCateNum.setText("");
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} else {

					}
				} else {

				}
			} else {
				txtCateName.setText("");
				txtCateNum.setText("");
				isFirstClick = false;
			}
		});

		btnCateDelete.addActionListener(event -> {
			if (t != -1) {
				if (t != m) {
					try {
						DefaultTableModel model = new DefaultTableModel();
						String sql = "delete from category where cateid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableCate.getValueAt(t, 0));

						// Display query string
						txtCateResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						model = (DefaultTableModel) tableCate.getModel();
						model.removeRow(t);
						m = t;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnCateUpdate.addActionListener(event -> {

			if (!RegexClass.isEmpty(txtCateName.getText())) {
				if (!RegexClass.isEmpty(txtCateNum.getText())) {
					try {
						DefaultTableModel model = new DefaultTableModel();
						String sql = "update category set catename = ?, catenum = ? where cateid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtCateName.getText());
						ps.setInt(2, Integer.parseInt(txtCateNum.getText()));
						ps.setInt(3, (int) tableCate.getValueAt(t, 0));

						// Display query string
						txtCateResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						tableCate.setValueAt((int) tableCate.getValueAt(t, 0), t, 0);
						tableCate.setValueAt(txtCateName.getText(), t, 1);
						tableCate.setValueAt(txtCateNum.getText(), t, 2);

					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {

				}
			} else {

			}
		});
		//

		// Inital
		rdQuery.setSelected(true);
		btnBookDelete.setEnabled(false);
		btnBookInsert.setEnabled(false);
		btnBookUpdate.setEnabled(false);
		txtBookName.setEnabled(false);
		txtBookRe.setEnabled(false);
		txtBookYear.setEnabled(false);
		txtBookMan.setEnabled(false);
		txtBookNum.setEnabled(false);
		cbBookCate.setEnabled(false);
		cbAuthor.setEnabled(false);

		JPanel panelAuhtor = new JPanel();
		tabbedPane.addTab("Author", null, panelAuhtor, null);
		panelAuhtor.setLayout(null);

		JLabel lblAuhtorTtile = new JLabel("Author Management");
		lblAuhtorTtile.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAuhtorTtile.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuhtorTtile.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAuhtorTtile.setBounds(362, 11, 305, 42);
		panelAuhtor.add(lblAuhtorTtile);

		JLabel label_2 = new JLabel("Query");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 64, 297, 33);
		panelAuhtor.add(label_2);

		JScrollPane scrollPane_21 = new JScrollPane();
		scrollPane_21.setBounds(13, 111, 287, 100);
		panelAuhtor.add(scrollPane_21);

		JTextArea txtAuthorQuery = new JTextArea();
		scrollPane_21.setViewportView(txtAuthorQuery);
		txtAuthorQuery.setLineWrap(true);

		JPanel panel_9 = new JPanel();
		panel_9.setBounds(10, 260, 1034, 42);
		panelAuhtor.add(panel_9);
		panel_9.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdAuthorQuery = new JRadioButton("Query");
		buttonGroup_5.add(rdAuthorQuery);
		rdAuthorQuery.setSelected(true);
		panel_9.add(rdAuthorQuery);

		JRadioButton rdAuthorButton = new JRadioButton("Buton");
		buttonGroup_5.add(rdAuthorButton);
		panel_9.add(rdAuthorButton);

		JScrollPane scrollPane_19 = new JScrollPane();
		scrollPane_19.setBounds(331, 316, 710, 239);
		panelAuhtor.add(scrollPane_19);

		/*
		 * DefaultTableModel modelDe = new DefaultTableModel(); Object[] rowDe = new
		 * Object[2]; modelDe.setColumnIdentifiers(department.getTitles());
		 * tableDepartment = new JTable(); tableDepartment.setModel(modelDe);
		 * 
		 * for (Department c : department.getAll()) { rowDe[0] = c.getId(); rowDe[1] =
		 * c.getName(); modelDe.addRow(rowDe); tableDepartment.setModel(modelDe); }
		 */
		DefaultTableModel modelAu = new DefaultTableModel();
		Object[] rowAu = new Object[3];
		modelAu.setColumnIdentifiers(author.getTitles());
		tableAuthor = new JTable();
		tableAuthor.setModel(modelAu);

		for (Author c : author.getAll()) {
			rowAu[0] = c.getId();
			rowAu[1] = c.getName();
			rowAu[2] = c.getDate();
			modelAu.addRow(rowAu);
			tableAuthor.setModel(modelAu);
		}

		scrollPane_19.setViewportView(tableAuthor);

		txtAuthorName = new JTextField();
		txtAuthorName.setEnabled(false);
		txtAuthorName.setColumns(10);
		txtAuthorName.setBounds(115, 313, 192, 20);
		panelAuhtor.add(txtAuthorName);

		JLabel lblAuthorName = new JLabel("Author name");
		lblAuthorName.setBounds(10, 316, 95, 14);
		panelAuhtor.add(lblAuthorName);

		JLabel label_4 = new JLabel("Date of birth");
		label_4.setBounds(10, 347, 95, 14);
		panelAuhtor.add(label_4);

		txtAuthorDate = new JTextField();
		txtAuthorDate.setEnabled(false);
		txtAuthorDate.setColumns(10);
		txtAuthorDate.setBounds(115, 344, 192, 20);
		panelAuhtor.add(txtAuthorDate);

		JScrollPane scrollPane_20 = new JScrollPane();
		scrollPane_20.setBounds(335, 112, 706, 100);
		panelAuhtor.add(scrollPane_20);

		JTextArea txtAuthorResult = new JTextArea();
		scrollPane_20.setViewportView(txtAuthorResult);
		txtAuthorResult.setLineWrap(true);

		JPanel panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_10.setBounds(10, 566, 1034, 61);
		panelAuhtor.add(panel_10);

		JButton btnAuthorInsert = new JButton("Insert");
		btnAuthorInsert.setEnabled(false);
		btnAuthorInsert.setBounds(147, 5, 148, 45);
		panel_10.add(btnAuthorInsert);

		JButton btnAuthorUpdate = new JButton("Update");
		btnAuthorUpdate.setEnabled(false);
		btnAuthorUpdate.setBounds(442, 5, 148, 45);
		panel_10.add(btnAuthorUpdate);

		JButton btnAuthorDelete = new JButton("Delete");
		btnAuthorDelete.setEnabled(false);
		btnAuthorDelete.setBounds(737, 5, 148, 45);
		panel_10.add(btnAuthorDelete);

		JButton btnAuthorRun = new JButton("Run");
		btnAuthorRun.setBounds(115, 222, 89, 23);
		panelAuhtor.add(btnAuthorRun);

		JPanel panelStudent = new JPanel();
		tabbedPane.addTab("Student", null, panelStudent, null);
		panelStudent.setLayout(null);

		JLabel lblStudentTitle = new JLabel("Student Management");
		lblStudentTitle.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStudentTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudentTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblStudentTitle.setBounds(354, 11, 305, 42);
		panelStudent.add(lblStudentTitle);

		JLabel lblStudentQuery = new JLabel("Query");
		lblStudentQuery.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudentQuery.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblStudentQuery.setBounds(10, 35, 297, 33);
		panelStudent.add(lblStudentQuery);

		JScrollPane scrollPane_10 = new JScrollPane();
		scrollPane_10.setBounds(11, 80, 291, 104);
		panelStudent.add(scrollPane_10);

		JTextArea txtStudentQuery = new JTextArea();
		scrollPane_10.setViewportView(txtStudentQuery);
		txtStudentQuery.setLineWrap(true);

		JScrollPane scrollPane_11 = new JScrollPane();
		scrollPane_11.setBounds(333, 81, 710, 104);
		panelStudent.add(scrollPane_11);

		JTextArea txtStudentResult = new JTextArea();
		scrollPane_11.setViewportView(txtStudentResult);
		txtStudentResult.setLineWrap(true);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(10, 224, 1034, 42);
		panelStudent.add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdStudentQuery = new JRadioButton("Query");
		buttonGroup_2.add(rdStudentQuery);
		rdStudentQuery.setSelected(true);
		panel_4.add(rdStudentQuery);

		JRadioButton rdStudentButton = new JRadioButton("Buton");
		buttonGroup_2.add(rdStudentButton);
		panel_4.add(rdStudentButton);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBounds(10, 530, 1034, 61);
		panelStudent.add(panel_5);

		JButton btnStudentInsert = new JButton("Insert");
		btnStudentInsert.setEnabled(false);
		btnStudentInsert.setBounds(147, 5, 148, 45);
		panel_5.add(btnStudentInsert);

		JButton btnStudentUpdate = new JButton("Update");
		btnStudentUpdate.setEnabled(false);
		btnStudentUpdate.setBounds(442, 5, 148, 45);
		panel_5.add(btnStudentUpdate);

		JButton btnStudentDelete = new JButton("Delete");
		btnStudentDelete.setEnabled(false);
		btnStudentDelete.setBounds(737, 5, 148, 45);
		panel_5.add(btnStudentDelete);

		JScrollPane scrollPane_12 = new JScrollPane();
		scrollPane_12.setBounds(329, 278, 714, 241);
		panelStudent.add(scrollPane_12);

		DefaultTableModel modelStu = new DefaultTableModel();
		Object[] rowStu = new Object[6];
		modelStu.setColumnIdentifiers(student.getTitles());
		tableStudent = new JTable();
		tableStudent.setModel(modelStu);
		for (Student s : student.getAll()) {
			rowStu[0] = s.getId();
			rowStu[1] = s.getDid();
			rowStu[2] = s.getFname();
			rowStu[3] = s.getLname();
			rowStu[4] = s.getMname();
			rowStu[5] = s.getDate();
			modelStu.addRow(rowStu);
			tableStudent.setModel(modelStu);
		}
		scrollPane_12.setViewportView(tableStudent);

		txtStudentFname = new JTextField();
		txtStudentFname.setColumns(10);
		txtStudentFname.setBounds(115, 277, 192, 20);
		panelStudent.add(txtStudentFname);

		JLabel lblStudentFname = new JLabel("First name");
		lblStudentFname.setBounds(10, 280, 95, 14);
		panelStudent.add(lblStudentFname);

		JLabel lblMiddleName = new JLabel("Middle name");
		lblMiddleName.setBounds(10, 311, 95, 14);
		panelStudent.add(lblMiddleName);

		txtStudentLname = new JTextField();
		txtStudentLname.setColumns(10);
		txtStudentLname.setBounds(115, 308, 192, 20);
		panelStudent.add(txtStudentLname);

		txtStudentDate = new JTextField();
		txtStudentDate.setColumns(10);
		txtStudentDate.setBounds(115, 370, 192, 20);
		panelStudent.add(txtStudentDate);

		txtStudentMname = new JTextField();
		txtStudentMname.setColumns(10);
		txtStudentMname.setBounds(115, 339, 192, 20);
		panelStudent.add(txtStudentMname);

		JComboBox<String> cbStudentDe = new JComboBox<String>();
		cbStudentDe.setBounds(115, 401, 192, 20);
		for (String s : department.getNames())
			cbStudentDe.addItem(s);
		panelStudent.add(cbStudentDe);

		JLabel lblLastName = new JLabel("Last name");
		lblLastName.setBounds(10, 336, 95, 14);
		panelStudent.add(lblLastName);

		JLabel lblDateOfBirth = new JLabel("Date of birth");
		lblDateOfBirth.setBounds(10, 367, 95, 14);
		panelStudent.add(lblDateOfBirth);

		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setBounds(10, 401, 95, 14);
		panelStudent.add(lblDepartment);

		JButton btnStudentRun = new JButton("Run");
		btnStudentRun.setBounds(115, 190, 89, 23);
		panelStudent.add(btnStudentRun);

		JPanel panelStaff = new JPanel();
		tabbedPane.addTab("Staff", null, panelStaff, null);
		panelStaff.setLayout(null);

		JLabel lblStaffTitle = new JLabel("Satff Management");
		lblStaffTitle.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStaffTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblStaffTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblStaffTitle.setBounds(354, 11, 305, 42);
		panelStaff.add(lblStaffTitle);

		JLabel label_1 = new JLabel("Query");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_1.setBounds(10, 64, 297, 33);
		panelStaff.add(label_1);

		JScrollPane scrollPane_16 = new JScrollPane();
		scrollPane_16.setBounds(12, 110, 289, 102);
		panelStaff.add(scrollPane_16);

		JTextArea txtStaffQuery = new JTextArea();
		scrollPane_16.setViewportView(txtStaffQuery);
		txtStaffQuery.setLineWrap(true);

		JPanel panel_7 = new JPanel();
		panel_7.setBounds(10, 260, 1034, 42);
		panelStaff.add(panel_7);
		panel_7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdStaffQuery = new JRadioButton("Query");
		buttonGroup_4.add(rdStaffQuery);
		rdStaffQuery.setSelected(true);
		panel_7.add(rdStaffQuery);

		JRadioButton rdStaffButton = new JRadioButton("Buton");
		buttonGroup_4.add(rdStaffButton);
		panel_7.add(rdStaffButton);

		JScrollPane scrollPane_18 = new JScrollPane();
		scrollPane_18.setBounds(330, 315, 712, 240);
		panelStaff.add(scrollPane_18);

		DefaultTableModel modelStaff = new DefaultTableModel();
		Object[] rowStaff = new Object[3];
		tableStaff = new JTable();
		modelStaff.setColumnIdentifiers(staff.getTitles());
		tableStaff.setModel(modelStaff);

		for (Staff s : staff.getAll()) {
			rowStaff[0] = s.getId();
			rowStaff[1] = s.getName();
			rowStaff[2] = s.getDate();
			modelStaff.addRow(rowStaff);
			tableStaff.setModel(modelStaff);
		}
		scrollPane_18.setViewportView(tableStaff);

		txtStaffName = new JTextField();
		txtStaffName.setEnabled(false);
		txtStaffName.setColumns(10);
		txtStaffName.setBounds(115, 313, 192, 20);
		panelStaff.add(txtStaffName);

		JLabel lblStaffName = new JLabel("Staff name");
		lblStaffName.setBounds(10, 316, 95, 14);
		panelStaff.add(lblStaffName);

		JLabel lblDateOfBirth_1 = new JLabel("Date of birth");
		lblDateOfBirth_1.setBounds(10, 347, 95, 14);
		panelStaff.add(lblDateOfBirth_1);

		txtStaffDate = new JTextField();
		txtStaffDate.setEnabled(false);
		txtStaffDate.setColumns(10);
		txtStaffDate.setBounds(115, 344, 192, 20);
		panelStaff.add(txtStaffDate);

		JScrollPane scrollPane_17 = new JScrollPane();
		scrollPane_17.setBounds(334, 111, 708, 102);
		panelStaff.add(scrollPane_17);

		JTextArea txtStaffResult = new JTextArea();
		scrollPane_17.setViewportView(txtStaffResult);
		txtStaffResult.setLineWrap(true);

		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBounds(10, 566, 1034, 61);
		panelStaff.add(panel_8);

		JButton btnStaffInsert = new JButton("Insert");
		btnStaffInsert.setEnabled(false);
		btnStaffInsert.setBounds(147, 5, 148, 45);
		panel_8.add(btnStaffInsert);

		JButton btnStaffUpdate = new JButton("Update");
		btnStaffUpdate.setEnabled(false);
		btnStaffUpdate.setBounds(442, 5, 148, 45);
		panel_8.add(btnStaffUpdate);

		JButton btnStaffDelete = new JButton("Delete");
		btnStaffDelete.setEnabled(false);
		btnStaffDelete.setBounds(737, 5, 148, 45);
		panel_8.add(btnStaffDelete);

		JButton btnStaffRun = new JButton("Run");
		btnStaffRun.setBounds(115, 226, 89, 23);
		panelStaff.add(btnStaffRun);

		JPanel panelDepartment = new JPanel();
		tabbedPane.addTab("Department", null, panelDepartment, null);
		panelDepartment.setLayout(null);

		JLabel lblDepartmentManagement = new JLabel("Department Management");
		lblDepartmentManagement.setHorizontalTextPosition(SwingConstants.CENTER);
		lblDepartmentManagement.setHorizontalAlignment(SwingConstants.CENTER);
		lblDepartmentManagement.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDepartmentManagement.setBounds(354, 11, 305, 42);
		panelDepartment.add(lblDepartmentManagement);

		JLabel label = new JLabel("Query");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setBounds(10, 64, 297, 33);
		panelDepartment.add(label);

		JScrollPane scrollPane_14 = new JScrollPane();
		scrollPane_14.setBounds(12, 110, 289, 102);
		panelDepartment.add(scrollPane_14);

		JTextArea txtDepartQuery = new JTextArea();
		scrollPane_14.setViewportView(txtDepartQuery);
		txtDepartQuery.setLineWrap(true);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 260, 1034, 42);
		panelDepartment.add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JRadioButton rdDepartmentQuery = new JRadioButton("Query");
		buttonGroup_3.add(rdDepartmentQuery);
		rdDepartmentQuery.setSelected(true);
		panel_3.add(rdDepartmentQuery);

		JRadioButton rdDepartmentButton = new JRadioButton("Buton");
		buttonGroup_3.add(rdDepartmentButton);
		panel_3.add(rdDepartmentButton);

		JScrollPane scrollPane_13 = new JScrollPane();
		scrollPane_13.setBounds(330, 315, 712, 240);
		panelDepartment.add(scrollPane_13);

		DefaultTableModel modelDe = new DefaultTableModel();
		Object[] rowDe = new Object[2];
		modelDe.setColumnIdentifiers(department.getTitles());
		tableDepartment = new JTable();
		tableDepartment.setModel(modelDe);

		for (Department c : department.getAll()) {
			rowDe[0] = c.getId();
			rowDe[1] = c.getName();
			modelDe.addRow(rowDe);
			tableDepartment.setModel(modelDe);
		}

		scrollPane_13.setViewportView(tableDepartment);

		txtDeName = new JTextField();
		txtDeName.setColumns(10);
		txtDeName.setBounds(115, 313, 192, 20);
		panelDepartment.add(txtDeName);

		JLabel lblDeoartmentName = new JLabel("Department name");
		lblDeoartmentName.setBounds(10, 316, 95, 14);
		panelDepartment.add(lblDeoartmentName);

		JScrollPane scrollPane_15 = new JScrollPane();
		scrollPane_15.setBounds(334, 111, 708, 102);
		panelDepartment.add(scrollPane_15);

		JTextArea txtDepartmentResult = new JTextArea();
		scrollPane_15.setViewportView(txtDepartmentResult);
		txtDepartmentResult.setLineWrap(true);

		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBounds(10, 566, 1034, 61);
		panelDepartment.add(panel_6);

		JButton btnDeInsert = new JButton("Insert");
		btnDeInsert.setBounds(147, 5, 148, 45);
		panel_6.add(btnDeInsert);

		JButton btnDeUpdate = new JButton("Update");
		btnDeUpdate.setBounds(442, 5, 148, 45);
		panel_6.add(btnDeUpdate);

		JButton btnDeDelete = new JButton("Delete");
		btnDeDelete.setBounds(737, 5, 148, 45);
		panel_6.add(btnDeDelete);

		JButton btnDeRun = new JButton("Run");
		btnDeRun.setBounds(115, 223, 89, 23);
		panelDepartment.add(btnDeRun);

		// Events

		tableDepartment.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				isFirstClickDe = true;
				rowDe1 = tableDepartment.getSelectedRow();
				rowDe2 = -1;
				txtDeName.setText((String) tableDepartment.getValueAt(rowDe1, 1));
			}
		});

		btnDeRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtDepartQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtDepartQuery.getText().trim());
					// insert
					if (txtDepartQuery.getText().trim().substring(0, 6).equalsIgnoreCase("select")) {
						rs = ps.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (meta.getColumnName(i).toUpperCase() + "\t\t");
						s += "\n";
						while (rs.next()) {
							for (int i = 1; i < meta.getColumnCount() + 1; i++)
								s += (rs.getString(i) + "\t\t");
							s += "\n";
						}
						txtDepartmentResult.setText(s);
					} else {
						ps.executeUpdate();
						txtDepartmentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		btnDeInsert.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtDeName.getText())) {
				if (!department.isExist(txtDeName.getText().trim())) {
					try {
						String sql = "insert into department values(null, ?)";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtDeName.getText().trim());
						ps.executeUpdate();
						txtDepartmentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();

						DefaultTableModel modelDeInsert = new DefaultTableModel();
						Object[] rowDeInsert = new Object[2];
						modelDeInsert.setColumnIdentifiers(department.getTitles());
						for (Department c : department.getAll()) {
							rowDeInsert[0] = c.getId();
							rowDeInsert[1] = c.getName();
							modelDeInsert.addRow(rowDeInsert);
							tableDepartment.setModel(modelDeInsert);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					txtDepartmentResult.setText("Department name already exists!!!");
				}
			}
		});

		btnDeUpdate.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtDeName.getText())) {
				if (!department.isExist(txtDeName.getText().trim())) {
					try {
						String sql = "update department set dname = ? where did = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtDeName.getText().trim());
						ps.setInt(2, (int) tableDepartment.getValueAt(rowDe1, 0));
						ps.executeUpdate();
						txtDepartmentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();
						DefaultTableModel modelDeUpdate = new DefaultTableModel();
						Object[] rowDeUpdate = new Object[2];
						modelDeUpdate.setColumnIdentifiers(department.getTitles());
						for (Department c : department.getAll()) {
							rowDeUpdate[0] = c.getId();
							rowDeUpdate[1] = c.getName();
							modelDeUpdate.addRow(rowDeUpdate);
							tableDepartment.setModel(modelDeUpdate);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					txtDepartmentResult.setText("Department name already exists!!!");
				}
			}
		});

		btnDeDelete.addActionListener(event -> {
			if (rowDe1 != -1) {
				if (rowDe1 != rowDe2) {
					try {
						DefaultTableModel modelDeDelete = new DefaultTableModel();
						String sql = "delete from department where did = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableDepartment.getValueAt(rowDe1, 0));

						// Display query string
						txtDepartmentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						modelDeDelete = (DefaultTableModel) tableDepartment.getModel();
						modelDeDelete.removeRow(rowDe1);
						rowDe2 = rowDe1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		rdDepartmentQuery.addActionListener(event -> {
			txtDeName.setEnabled(false);
			btnDeInsert.setEnabled(false);
			btnDeUpdate.setEnabled(false);
			btnDeDelete.setEnabled(false);

			btnDeRun.setEnabled(true);
		});

		rdDepartmentButton.addActionListener(event -> {
			txtDeName.setEnabled(true);
			btnDeInsert.setEnabled(true);
			btnDeUpdate.setEnabled(true);
			btnDeDelete.setEnabled(true);

			btnDeRun.setEnabled(false);
		});

		tableStudent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				isFirstClickStu = true;
				rowStu1 = tableStudent.getSelectedRow();
				rowStu2 = -1;
				txtStudentFname.setText((String) tableStudent.getValueAt(rowStu1, 2));
				txtStudentLname.setText((String) tableStudent.getValueAt(rowStu1, 3));
				txtStudentMname.setText((String) tableStudent.getValueAt(rowStu1, 4));
				txtStudentDate.setText((String) tableStudent.getValueAt(rowStu1, 5));
				String sql = "select dname from department where did = ?";
				Connection cn = Database.getConnetion();
				try {
					PreparedStatement ps = cn.prepareStatement(sql);
					ps.setInt(1, Integer.parseInt(tableStudent.getValueAt(rowStu1, 1) + ""));
					ResultSet rs = ps.executeQuery();

					if (rs.next())
						cbStudentDe.setSelectedItem(rs.getString(1));
					rs.close();
					ps.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		rdStudentButton.addActionListener(event -> {
			txtStudentFname.setEnabled(true);
			txtStudentLname.setEnabled(true);
			txtStudentMname.setEnabled(true);
			txtStudentDate.setEnabled(true);
			cbStudentDe.setEnabled(true);
			btnStudentDelete.setEnabled(true);
			btnStudentInsert.setEnabled(true);
			btnStudentUpdate.setEnabled(true);

			btnStudentRun.setEnabled(false);
		});

		rdStudentQuery.addActionListener(event -> {
			txtStudentFname.setEnabled(false);
			txtStudentLname.setEnabled(false);
			txtStudentMname.setEnabled(false);
			txtStudentDate.setEnabled(false);
			cbStudentDe.setEnabled(false);
			btnStudentDelete.setEnabled(false);
			btnStudentInsert.setEnabled(false);
			btnStudentUpdate.setEnabled(false);

			btnStudentRun.setEnabled(true);
		});

		btnStudentInsert.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStudentFname.getText())) {
				if (!RegexClass.isEmpty(txtStudentLname.getText())) {
					if (!RegexClass.isEmpty(txtStudentMname.getText())) {
						if (!RegexClass.isEmpty(txtStudentDate.getText())) {
							try {

								String sql = "insert into student(did, fname, lname, mname, bod) values (?,?,?,?,?)";
								cn = new MyConnect().getcn();
								ps = cn.prepareStatement(sql);
								ps.setInt(1, department.getIdByName((String) cbStudentDe.getSelectedItem()));
								ps.setString(2, txtStudentFname.getText());
								ps.setString(3, txtStudentLname.getText());
								ps.setString(4, txtStudentMname.getText());
								ps.setString(5, txtStudentDate.getText());
								ps.executeUpdate();
								txtStudentResult.setText("Query is used : \n"
										+ ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
								ps.close();

								DefaultTableModel modelStuInsert = new DefaultTableModel();
								Object[] rowStuInsert = new Object[6];
								modelStuInsert.setColumnIdentifiers(student.getTitles());
								for (Student s : student.getAll()) {
									rowStuInsert[0] = s.getId();
									rowStuInsert[1] = s.getDid();
									rowStuInsert[2] = s.getFname();
									rowStuInsert[3] = s.getLname();
									rowStuInsert[4] = s.getMname();
									rowStuInsert[5] = s.getDate();
									modelStuInsert.addRow(rowStuInsert);
									tableStudent.setModel(modelStuInsert);
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		});

		btnStudentDelete.addActionListener(event -> {
			if (rowStu1 != -1) {
				if (rowStu1 != rowStu2) {
					try {
						DefaultTableModel modelStuDelete = new DefaultTableModel();
						String sql = "delete from student where sid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableDepartment.getValueAt(rowStu1, 0));
						ps.executeUpdate();
						// Display query string
						txtDepartmentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						modelStuDelete = (DefaultTableModel) tableStudent.getModel();
						modelStuDelete.removeRow(rowStu1);
						rowStu2 = rowStu1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnStudentUpdate.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStudentFname.getText())) {
				if (!RegexClass.isEmpty(txtStudentLname.getText())) {
					if (!RegexClass.isEmpty(txtStudentMname.getText())) {
						if (!RegexClass.isEmpty(txtStudentDate.getText())) {
							try {
								String sql = "update student set did = ?, fname = ?, lname = ?, mname = ?, bod = ? where sid = ?";
								cn = new MyConnect().getcn();
								ps = cn.prepareStatement(sql);
								ps.setInt(1, department.getIdByName((String) cbStudentDe.getSelectedItem()));
								ps.setString(2, txtStudentFname.getText());
								ps.setString(3, txtStudentLname.getText());
								ps.setString(4, txtStudentMname.getText());
								ps.setString(5, txtStudentDate.getText());
								ps.setInt(6, (int) tableStudent.getValueAt(rowStu1, 0));
								ps.executeUpdate();
								txtStudentResult.setText("Query is used : \n"
										+ ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
								ps.close();

								DefaultTableModel modelStuInsert = new DefaultTableModel();
								Object[] rowStuInsert = new Object[6];
								modelStuInsert.setColumnIdentifiers(student.getTitles());
								for (Student s : student.getAll()) {
									rowStuInsert[0] = s.getId();
									rowStuInsert[1] = s.getDid();
									rowStuInsert[2] = s.getFname();
									rowStuInsert[3] = s.getLname();
									rowStuInsert[4] = s.getMname();
									rowStuInsert[5] = s.getDate();
									modelStuInsert.addRow(rowStuInsert);
									tableStudent.setModel(modelStuInsert);
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		});

		rdAuthorQuery.addActionListener(event -> {
			txtAuthorName.setEnabled(false);
			txtAuthorDate.setEnabled(false);
			btnAuthorDelete.setEnabled(false);
			btnAuthorInsert.setEnabled(false);
			btnAuthorUpdate.setEnabled(false);

			btnAuthorRun.setEnabled(true);
		});

		rdAuthorButton.addActionListener(event -> {
			txtAuthorName.setEnabled(true);
			txtAuthorDate.setEnabled(true);
			btnAuthorDelete.setEnabled(true);
			btnAuthorInsert.setEnabled(true);
			btnAuthorUpdate.setEnabled(true);

			btnAuthorRun.setEnabled(false);
		});

		tableAuthor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				isFirstClickAu = true;
				rowAu1 = tableAuthor.getSelectedRow();
				rowAu2 = -1;
				txtAuthorName.setText((String) tableAuthor.getValueAt(rowAu1, 1));
				txtAuthorDate.setText((String) tableAuthor.getValueAt(rowAu1, 2));
			}
		});

		btnAuthorInsert.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtAuthorName.getText())) {
				if (!RegexClass.isEmpty(txtAuthorDate.getText().trim())) {
					try {
						String sql = "insert into author(aname, bod) values(?,?)";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtAuthorName.getText().trim());
						ps.setString(2, txtAuthorDate.getText().trim());
						ps.executeUpdate();
						txtAuthorResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();

						DefaultTableModel modelAuInsert = new DefaultTableModel();
						Object[] rowAuInsert = new Object[3];
						modelAuInsert.setColumnIdentifiers(author.getTitles());
						for (Author c : author.getAll()) {
							rowAuInsert[0] = c.getId();
							rowAuInsert[1] = c.getName();
							rowAuInsert[2] = c.getDate();
							modelAuInsert.addRow(rowAuInsert);
							tableAuthor.setModel(modelAuInsert);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
				}
			}
		});

		btnAuthorDelete.addActionListener(event -> {
			if (rowAu1 != -1) {
				if (rowAu1 != rowAu2) {
					try {
						DefaultTableModel modelAuDelete = new DefaultTableModel();
						String sql = "delete from author where aid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableAuthor.getValueAt(rowAu1, 0));
						ps.executeUpdate();
						// Display query string
						txtAuthorResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						modelAuDelete = (DefaultTableModel) tableAuthor.getModel();
						modelAuDelete.removeRow(rowAu1);
						rowAu2 = rowAu1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnAuthorUpdate.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtAuthorName.getText())) {
				if (!RegexClass.isEmpty(txtAuthorDate.getText().trim())) {
					try {
						String sql = "update author set aname = ?, bod = ? where aid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtAuthorName.getText().trim());
						ps.setString(2, txtAuthorDate.getText().trim());
						ps.setInt(3, (int) tableAuthor.getValueAt(rowAu1, 0));
						ps.executeUpdate();
						txtAuthorResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();

						DefaultTableModel modelAuUpdate = new DefaultTableModel();
						Object[] rowAupdate = new Object[3];
						modelAuUpdate.setColumnIdentifiers(author.getTitles());
						for (Author c : author.getAll()) {
							rowAupdate[0] = c.getId();
							rowAupdate[1] = c.getName();
							rowAupdate[2] = c.getDate();
							modelAuUpdate.addRow(rowAupdate);
							tableAuthor.setModel(modelAuUpdate);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
				}
			}
		});

		rdStaffQuery.addActionListener(event -> {
			txtStaffName.setEnabled(false);
			txtStaffDate.setEnabled(false);
			btnStaffDelete.setEnabled(false);
			btnStaffInsert.setEnabled(false);
			btnStaffUpdate.setEnabled(false);

			btnStaffRun.setEnabled(true);
		});

		rdStaffButton.addActionListener(event -> {
			txtStaffName.setEnabled(true);
			txtStaffDate.setEnabled(true);
			btnStaffDelete.setEnabled(true);
			btnStaffInsert.setEnabled(true);
			btnStaffUpdate.setEnabled(true);

			btnStaffRun.setEnabled(false);
		});

		tableStaff.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				isFirstClickSta = true;
				rowSta1 = tableStaff.getSelectedRow();
				rowSta2 = -1;
				txtStaffName.setText((String) tableStaff.getValueAt(rowSta1, 1));
				txtStaffDate.setText((String) tableStaff.getValueAt(rowSta1, 2));
			}
		});

		btnStaffInsert.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStaffName.getText())) {
				if (!RegexClass.isEmpty(txtStaffDate.getText().trim())) {
					try {
						String sql = "insert into staff(stname, bod) values(?,?)";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtStaffName.getText().trim());
						ps.setString(2, txtStaffDate.getText().trim());
						ps.executeUpdate();
						txtStaffResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();

						DefaultTableModel modelStaInsert = new DefaultTableModel();
						Object[] rowStaInsert = new Object[3];
						modelStaInsert.setColumnIdentifiers(staff.getTitles());
						for (Staff c : staff.getAll()) {
							rowStaInsert[0] = c.getId();
							rowStaInsert[1] = c.getName();
							rowStaInsert[2] = c.getDate();
							modelStaInsert.addRow(rowStaInsert);
							tableStaff.setModel(modelStaInsert);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
				}
			}
		});

		btnStaffDelete.addActionListener(event -> {
			if (rowSta1 != -1) {
				if (rowSta1 != rowSta2) {
					try {
						DefaultTableModel modelStaDelete = new DefaultTableModel();
						String sql = "delete from staff where stid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setInt(1, (int) tableStaff.getValueAt(rowSta1, 0));
						ps.executeUpdate();
						// Display query string
						txtStaffResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						//

						ps.executeUpdate();
						ps.close();
						modelStaDelete = (DefaultTableModel) tableStaff.getModel();
						modelStaDelete.removeRow(rowSta1);
						rowSta2 = rowSta1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnStaffUpdate.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStaffName.getText())) {
				if (!RegexClass.isEmpty(txtStaffDate.getText().trim())) {
					try {
						String sql = "update staff set stname = ?, bod = ? where stid = ?";
						cn = new MyConnect().getcn();
						ps = cn.prepareStatement(sql);
						ps.setString(1, txtStaffName.getText().trim());
						ps.setString(2, txtStaffDate.getText().trim());
						ps.setInt(3, (int) tableStaff.getValueAt(rowSta1, 0));
						ps.executeUpdate();
						txtStaffResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
						ps.close();

						DefaultTableModel modelStaUpdate = new DefaultTableModel();
						Object[] rowStaUpdate = new Object[3];
						modelStaUpdate.setColumnIdentifiers(author.getTitles());
						for (Staff c : staff.getAll()) {
							rowStaUpdate[0] = c.getId();
							rowStaUpdate[1] = c.getName();
							rowStaUpdate[2] = c.getDate();
							modelStaUpdate.addRow(rowStaUpdate);
							tableStaff.setModel(modelStaUpdate);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
				}
			}
		});

		tableBorrowBook.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = tableBorrowBook.getSelectedRow();
				String preStr = txtBorrowBook.getText();
				Pattern pt = Pattern.compile(" " + tableBorrowBook.getValueAt(row, 0) + " ");
				Matcher mc = pt.matcher(preStr);
				if (!book.checkOutOfStock((int) tableBorrowBook.getValueAt(row, 0))) {
					if (mc.find()) {
						txtBorrowResult
								.setText("This book with id " + tableBorrowBook.getValueAt(row, 0) + " is selected!!!");
					} else {
						Pattern pt1 = Pattern.compile(tableBorrowBook.getValueAt(row, 0) + " ");
						Matcher mc1 = pt1.matcher(preStr);
						if (mc1.find()) {
							txtBorrowResult.setText(
									"This book with id " + tableBorrowBook.getValueAt(row, 0) + " is selected!!!");
						} else {
							Pattern pt2 = Pattern.compile(" " + tableBorrowBook.getValueAt(row, 0));
							Matcher mc2 = pt2.matcher(preStr);
							if (mc2.find()) {
								txtBorrowResult.setText(
										"This book with id " + tableBorrowBook.getValueAt(row, 0) + " is selected!!!");
							} else {
								txtBorrowBook.setText(preStr + " " + tableBorrowBook.getValueAt(row, 0));
							}
						}
					}
				} else {
					JOptionPane.showMessageDialog(null, "This book has been borrowed");
				}
			}
		});

		btnBorrow.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtBorrowBook.getText())) {
				try {
					String query = "";
					String[] books = txtBorrowBook.getText().split(" ");
					int reid = 0;
					String sql = "insert into record(sid, stid, borrowdate, duedate) values(?,?,?,?)";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(sql);
					ps.setInt(1, listSid.get(cbBorrowStudent.getSelectedIndex()));
					ps.setInt(2, listStid.get(cbBorrowStaff.getSelectedIndex()));
					ps.setString(3, staff.getCurDate());
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar c = Calendar.getInstance();
					try {
						// Setting the date to the given date
						c.setTime(sdf.parse(staff.getCurDate()));
					} catch (ParseException e) {
						e.printStackTrace();
					}

					// Number of Days to add
					c.add(Calendar.DAY_OF_MONTH, 7);
					// Date after adding the days to the given date
					ps.setString(4, sdf.format(c.getTime()));
					query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
					ps.executeUpdate();
					String s = "select max(reid) from record";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(s);
					query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
					rs = ps.executeQuery();
					if (rs.next())
						reid = rs.getInt(1);
					String sql1 = "insert into cardborrow(reid, bid) values(?,?)";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(sql1);
					for (String i : books) {
						if (!i.equalsIgnoreCase("")) {
							ps.setInt(1, reid);
							ps.setInt(2, Integer.parseInt(i));
							query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
							ps.executeUpdate();
						}
					}

					// update books
					String sql3 = "update book set isborrowed = ? where bid = ?";
					String sql4 = "select isborrowed from book where bid = ?";
					int isborrowed = 0;

					for (String i : books) {
						if (!i.equalsIgnoreCase("")) {
							ps = cn.prepareStatement(sql4);
							ps.setInt(1, Integer.parseInt(i));
							query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
							rs = ps.executeQuery();
							if (rs.next())
								isborrowed = rs.getInt(1);
							ps = cn.prepareStatement(sql3);
							ps.setInt(1, isborrowed + 1);
							ps.setInt(2, Integer.parseInt(i));
							query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
							ps.executeUpdate();
						}
					}
					rs.close();
					ps.close();
					txtBorrowBook.setText("");
					DefaultTableModel model2 = new DefaultTableModel();
					Object[] row2 = new Object[5];
					model2.setColumnIdentifiers(record.getTitles());
					tableRecord.setModel(modelRecord);

					for (Record r : record.getAll()) {
						row2[0] = r.getId();
						row2[1] = r.getSid();
						row2[2] = r.getStid();
						row2[3] = r.getBorrowdate();
						row2[4] = r.getDuedate();
						model2.addRow(row2);
						tableRecord.setModel(model2);
					}

					txtBorrowResult.setText("Queries is used : \n" + query);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			} else {
				txtBorrowResult.setText("Cart book is empty.");
			}
		});

		btnBorrowRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtBorrowQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtBorrowQuery.getText().trim());
					// insert
					if (txtBorrowQuery.getText().trim().substring(0, 6).equalsIgnoreCase("select")) {
						rs = ps.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (meta.getColumnName(i).toUpperCase() + "\t\t");
						s += "\n";
						while (rs.next()) {
							for (int i = 1; i < meta.getColumnCount() + 1; i++)
								s += (rs.getString(i) + "\t\t");
							s += "\n";
						}
						txtBorrowResult.setText(s);
					} else {
						ps.executeUpdate();
						txtBorrowResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		btnView.addActionListener(event -> {
			String card = "Card's #title# \nStaff : #staffName# \nStudent : #studentName# \nDepartment : #de# \nBooks : \n#books# \nFrom : #from# \nTo : #to#";
			Record re = new Record();
			String books = "";
			List<Integer> bids = new ArrayList<>();
			int reid = (int) tableRecord.getValueAt(tableRecord.getSelectedRow(), 0);
			String sql = "select * from record where reid = ?";
			String sql1 = "select bid from cardborrow where reid = ?";
			String sql2 = "select stname from staff where stid = ?";
			String sql3 = "select dname from department where did = ?";
			String sql4 = "select bname from book where bid = ?";
			String sql5 = "select fname, mname, lname from student where sid = ?";
			try {
				cn = new MyConnect().getcn();
				ps = cn.prepareStatement(sql);
				ps.setInt(1, reid);
				rs = ps.executeQuery();
				if (rs.next())
					re = new Record(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5));
				ps = cn.prepareStatement(sql1);
				ps.setInt(1, reid);
				rs = ps.executeQuery();
				while (rs.next())
					bids.add(rs.getInt(1));
				for (Integer i : bids) {
					if (bids.indexOf(i) != bids.size() - 1) {
						ps = cn.prepareStatement(sql4);
						ps.setInt(1, i);
						rs = ps.executeQuery();
						if (rs.next())
							books += (bids.indexOf(i) + 1 + ". " + rs.getString(1) + "\n");
					} else {
						ps = cn.prepareStatement(sql4);
						ps.setInt(1, i);
						rs = ps.executeQuery();
						if (rs.next())
							books += (bids.indexOf(i) + 1 + ". " + rs.getString(1));
					}
				}
				card = card.replace("#books#", books);
				ps = cn.prepareStatement(sql2);
				ps.setInt(1, re.getStid());
				rs = ps.executeQuery();
				if (rs.next())
					card = card.replace("#staffName#", rs.getString(1));
				ps = cn.prepareStatement(sql5);
				ps.setInt(1, re.getSid());
				rs = ps.executeQuery();
				if (rs.next()) {
					card = card.replace("#studentName#",
							rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
					card = card.replace("#title#", rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
				}
				ps = cn.prepareStatement(sql3);
				ps.setInt(1, re.getStid());
				rs = ps.executeQuery();
				if (rs.next())
					card = card.replace("#de#", rs.getString(1));
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			card = card.replace("#from#", re.getBorrowdate());
			card = card.replace("#to#", re.getDuedate());
			JOptionPane.showMessageDialog(this, card);
		});

		btnAuthorRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtAuthorQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtAuthorQuery.getText().trim());
					// insert
					if (txtAuthorQuery.getText().trim().substring(0, 6).equalsIgnoreCase("select")) {
						rs = ps.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (meta.getColumnName(i).toUpperCase() + "\t\t");
						s += "\n";
						while (rs.next()) {
							for (int i = 1; i < meta.getColumnCount() + 1; i++)
								s += (rs.getString(i) + "\t\t");
							s += "\n";
						}
						txtBorrowResult.setText(s);
					} else {
						ps.executeUpdate();
						txtAuthorResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		btnStudentRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStudentQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtStudentQuery.getText().trim());
					// insert
					if (txtStudentQuery.getText().trim().substring(0, 6).equalsIgnoreCase("select")) {
						rs = ps.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (meta.getColumnName(i).toUpperCase() + "\t\t");
						s += "\n";
						while (rs.next()) {
							for (int i = 1; i < meta.getColumnCount() + 1; i++)
								s += (rs.getString(i) + "\t\t");
							s += "\n";
						}
						txtStudentResult.setText(s);
					} else {
						ps.executeUpdate();
						txtStudentResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		btnStaffRun.addActionListener(event -> {
			if (!RegexClass.isEmpty(txtStaffQuery.getText())) {
				try {
					String s = "";
					cn = new MyConnect().getcn();
					ps = cn.prepareStatement(txtStaffQuery.getText().trim());
					// insert
					if (txtStaffQuery.getText().trim().substring(0, 6).equalsIgnoreCase("select")) {
						rs = ps.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						for (int i = 1; i < meta.getColumnCount() + 1; i++)
							s += (meta.getColumnName(i).toUpperCase() + "\t\t");
						s += "\n";
						while (rs.next()) {
							for (int i = 1; i < meta.getColumnCount() + 1; i++)
								s += (rs.getString(i) + "\t\t");
							s += "\n";
						}
						txtStaffResult.setText(s);
					} else {
						ps.executeUpdate();
						txtStaffResult.setText(
								"Query is used : \n" + ps.toString().substring(ps.toString().indexOf(":") + 1).trim());
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		btnReturn.addActionListener(event -> {
			String query = "";
			int reid = (int) tableRecord.getValueAt(tableRecord.getSelectedRow(), 0);
			List<Integer> books = new ArrayList<>();
			String sql1 = "select bid from cardborrow where reid = ?";
			String sql2 = "delete from cardborrow where bid = ?";
			String sql3 = "update book set isborrowed = ? where bid = ?";
			String sql4 = "select isborrowed from book where bid = ?";
			String sql5 = "delete from record where reid = ?";
			cn = new MyConnect().getcn();
			try {
				ps = cn.prepareStatement(sql1);
				ps.setInt(1, reid);
				query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
				rs = ps.executeQuery();
				while (rs.next())
					books.add(rs.getInt(1));
				int isborrowed = 0;

				for (Integer i : books) {
					ps = cn.prepareStatement(sql4);
					ps.setInt(1, i);
					query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
					rs = ps.executeQuery();
					if (rs.next())
						isborrowed = rs.getInt(1);
					ps = cn.prepareStatement(sql3);
					ps.setInt(1, isborrowed - 1);
					ps.setInt(2, i);
					query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
					ps.executeUpdate();
					ps = cn.prepareStatement(sql2);
					ps.setInt(1, i);
					query += ps.toString().substring(ps.toString().indexOf(":") + 1).trim() + "\n";
					ps.executeUpdate();
				}
				ps = cn.prepareStatement(sql5);
				ps.setInt(1, reid);
				ps.executeUpdate();
				rs.close();
				ps.close();
				txtBorrowResult.setText("Query is used : \n" + query);

				DefaultTableModel model5 = new DefaultTableModel();
				Object[] row5 = new Object[5];
				model5.setColumnIdentifiers(record.getTitles());
				tableRecord.setModel(modelRecord);

				for (Record r : record.getAll()) {
					row5[0] = r.getId();
					row5[1] = r.getSid();
					row5[2] = r.getStid();
					row5[3] = r.getBorrowdate();
					row5[4] = r.getDuedate();
					model5.addRow(row5);
					tableRecord.setModel(model5);
				}

				DefaultTableModel model6 = new DefaultTableModel();
				Object[] row6 = new Object[9];
				model6.setColumnIdentifiers(book.getTitles());
				tableBorrowBook.setModel(modelBorrow);

				for (Book b : book.getAll()) {
					row6[0] = b.getId();
					row6[1] = b.getName();
					row6[2] = b.getNum();
					row6[3] = b.getRepublish();
					row6[4] = b.getYear();
					row6[5] = b.getManufacturer();
					row6[6] = b.getAid();
					row6[7] = b.getCateid();
					row6[8] = b.getBorrowed();
					model6.addRow(row6);
					tableBorrowBook.setModel(model6);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});

		btnRemove.addActionListener(event -> {
			int row1 = tableBorrowBook.getSelectedRow();
			String preStr = txtBorrowBook.getText();
			Pattern pt = Pattern.compile(" " + tableBorrowBook.getValueAt(row1, 0) + " ");
			Matcher mc = pt.matcher(preStr);
			int count = 0;
			if (mc.find()) {
				txtBorrowBook.setText(preStr.replace(" " + tableBorrowBook.getValueAt(row1, 0) + " ", " "));
			} else {
				count++;
				Pattern pt1 = Pattern.compile(tableBorrowBook.getValueAt(row1, 0) + " ");
				Matcher mc1 = pt1.matcher(preStr);
				if (mc1.find()) {
					txtBorrowBook.setText(preStr.replace(tableBorrowBook.getValueAt(row1, 0) + " ", ""));
				} else {
					count++;
					Pattern pt2 = Pattern.compile(" " + tableBorrowBook.getValueAt(row1, 0));
					Matcher mc2 = pt2.matcher(preStr);
					if (mc2.find()) {
						txtBorrowBook.setText(preStr.replace(" " + tableBorrowBook.getValueAt(row1, 0), ""));
					} else {
						count++;
					}
				}
			}
			if (count == 3)
				JOptionPane.showMessageDialog(null, "This book does not exist in your cart!!!");
		});

		tabbedPane.addChangeListener(event -> {
			DefaultTableModel model6 = new DefaultTableModel();
			Object[] row6 = new Object[9];
			model6.setColumnIdentifiers(book.getTitles());
			tableBorrowBook.setModel(modelBorrow);

			for (Book b : book.getAll()) {
				row6[0] = b.getId();
				row6[1] = b.getName();
				row6[2] = b.getNum();
				row6[3] = b.getRepublish();
				row6[4] = b.getYear();
				row6[5] = b.getManufacturer();
				row6[6] = b.getAid();
				row6[7] = b.getCateid();
				row6[8] = b.getBorrowed();
				model6.addRow(row6);
				tableBorrowBook.setModel(model6);
			}

			DefaultTableModel model5 = new DefaultTableModel();
			Object[] row5 = new Object[5];
			model5.setColumnIdentifiers(record.getTitles());
			tableRecord.setModel(modelRecord);

			for (Record r : record.getAll()) {
				row5[0] = r.getId();
				row5[1] = r.getSid();
				row5[2] = r.getStid();
				row5[3] = r.getBorrowdate();
				row5[4] = r.getDuedate();
				model5.addRow(row5);
				tableRecord.setModel(model5);
			}

			DefaultTableModel model4 = new DefaultTableModel();
			Object[] row4 = new Object[5];
			model4.setColumnIdentifiers(record.getTitles());
			tableRecord.setModel(modelRecord);

			for (Record r : record.getAll()) {
				row4[0] = r.getId();
				row4[1] = r.getSid();
				row4[2] = r.getStid();
				row4[3] = r.getBorrowdate();
				row4[4] = r.getDuedate();
				model4.addRow(row4);
				tableRecord.setModel(model4);
			}

			DefaultTableModel model3 = new DefaultTableModel();
			Object[] row3 = new Object[3];
			model3.setColumnIdentifiers(author.getTitles());
			for (Staff c : staff.getAll()) {
				row3[0] = c.getId();
				row3[1] = c.getName();
				row3[2] = c.getDate();
				model3.addRow(row3);
				tableStaff.setModel(model3);
			}

			DefaultTableModel model2 = new DefaultTableModel();
			Object[] row2 = new Object[3];
			model2.setColumnIdentifiers(author.getTitles());
			for (Author c : author.getAll()) {
				row2[0] = c.getId();
				row2[1] = c.getName();
				row2[2] = c.getDate();
				model2.addRow(row2);
				tableAuthor.setModel(model2);
			}

			DefaultTableModel model1 = new DefaultTableModel();
			Object[] row1 = new Object[6];
			model1.setColumnIdentifiers(student.getTitles());
			for (Student s : student.getAll()) {
				row1[0] = s.getId();
				row1[1] = s.getDid();
				row1[2] = s.getFname();
				row1[3] = s.getLname();
				row1[4] = s.getMname();
				row1[5] = s.getDate();
				model1.addRow(row1);
				tableStudent.setModel(model1);
			}
			DefaultTableModel model7 = new DefaultTableModel();
			Object[] row7 = new Object[2];
			model7.setColumnIdentifiers(department.getTitles());
			for (Department c : department.getAll()) {
				row7[0] = c.getId();
				row7[1] = c.getName();
				model7.addRow(row7);
				tableDepartment.setModel(model7);
			} 
			
			DefaultTableModel model8 = new DefaultTableModel();
			Object[] row8 = new Object[book.getNumField()];
			model8.setColumnIdentifiers(book.getTitles());
			for (Book b : book.getAll()) {
				row8[0] = b.getId();
				row8[1] = b.getName();
				row8[2] = b.getNum();
				row8[3] = b.getRepublish();
				row8[4] = b.getYear();
				row8[5] = b.getManufacturer();
				row8[6] = b.getAid();
				row8[7] = b.getCateid();
				row8[8] = b.getBorrowed();
				model8.addRow(row8);
				tableBook.setModel(model8);
			}

		});
		//

		setSize(1113, 800);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void main(String[] args) {
		new Swing();

		/*
		 * String t = "1 2 13 15 16 17 111"; Pattern pt = Pattern.compile(" 2 ");
		 * Matcher mc = pt.matcher(t);
		 * 
		 * System.out.println(mc.find());
		 */

	}
}
