package pdm.library.swing;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class Library extends JFrame {
	private JTextField textField;
	public Library() {
		setSize(1246, 764);
		setTitle("Tabbed Pane");
        JTabbedPane jtp = new JTabbedPane();
        
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        JPanel jp4 = new JPanel();
        JLabel label1 = new JLabel();
        label1.setBounds(489, 5, 247, 37);
        label1.setFont(new Font("Tahoma", Font.PLAIN, 30));
        label1.setText("Book Management");
        JLabel label2 = new JLabel();
        label2.setFont(new Font("Tahoma", Font.PLAIN, 30));
        label2.setText("Category Management");
        JLabel label3 = new JLabel();
        label3.setFont(new Font("Tahoma", Font.PLAIN, 30));
        label3.setText("Card Management");
        JLabel label4 = new JLabel();
        label4.setFont(new Font("Tahoma", Font.PLAIN, 30));
        label4.setText("Borrow");
        jp1.setLayout(null);
        jp1.add(label1);
        jp2.add(label2);
        jp3.add(label3);
        jp4.add(label4);
        jtp.addTab("Book", jp1);
        
        JLabel lblNewLabel = new JLabel("Book name");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setBounds(10, 76, 122, 25);
        jp1.add(lblNewLabel);
        
        JLabel lblCategory = new JLabel("Category");
        lblCategory.setHorizontalAlignment(SwingConstants.CENTER);
        lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblCategory.setBounds(10, 112, 122, 25);
        jp1.add(lblCategory);
        
        textField = new JTextField();
        textField.setBounds(142, 82, 86, 20);
        jp1.add(textField);
        textField.setColumns(10);
        
        JComboBox comboBox = new JComboBox();
        comboBox.setBounds(142, 118, 86, 20);
        jp1.add(comboBox);
        jtp.addTab("Category", jp2);
        jtp.addTab("Card", jp3);
        jtp.addTab("Borrow", jp4);
        getContentPane().add(jtp);
        setVisible(true);
	}
	public static void main(String[] args) {
		Library l = new Library();
	}
}
