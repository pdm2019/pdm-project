package pdm.library.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Student {
	private Connection cn;
	private int id;
	private String fname;
	private String lname;
	private String mname;
	private String date;
	private int did;

	public Student(int id, String fname, String lname, String mname, String date, int did) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.mname = mname;
		this.date = date;
		this.did = did;
	}

	public Student() {
		super();
	}

	public Connection getCn() {
		return cn;
	}

	public void setCn(Connection cn) {
		this.cn = cn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public List<Student> getAll() {
		List<Student> list = new ArrayList<Student>();
		String sql = "select * from student";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Student(rs.getInt(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(2)));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public String[] getTitles() {
		String[] titles = null;
		String sql = "select * from student";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			titles = new String[meta.getColumnCount()];

			for (int i = 0; i < meta.getColumnCount(); i++)
				titles[i] = meta.getColumnName(i + 1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return titles;
	}

	public int getNumRecord() {
		int num = 0;
		String sql = "select count(*) from student";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				num = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	public String[] getNames() {
		String[] names = null;
		String sql = "select * from student";
		int i = 0;
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			names = new String[getNumRecord()];

			while (rs.next())
				names[i++] = (rs.getString(3) +" "+ rs.getString(4) +" "+ rs.getString(5));
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}
	
	public int getIdByName(String name) {
		int id = 0;
		String sql = "select did from department where dname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				id = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public List<Integer> getIds() {
		List<Integer> list = new ArrayList<Integer>();
		String sql = "select sid from student";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getInt(1));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}
