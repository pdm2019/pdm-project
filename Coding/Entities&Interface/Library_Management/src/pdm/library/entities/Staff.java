package pdm.library.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Staff {
	private Connection cn;
	private int id;
	private String name;
	private String date;

	public Staff(int id, String name, String date) {
		super();
		this.id = id;
		this.name = name;
		this.date = date;
	}

	public Staff() {
		super();
	}

	public Connection getCn() {
		return cn;
	}

	public void setCn(Connection cn) {
		this.cn = cn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<Staff> getAll() {
		List<Staff> list = new ArrayList<Staff>();
		String sql = "select * from staff";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Staff(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public String[] getTitles() {
		String[] titles = null;
		String sql = "select * from staff";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			titles = new String[meta.getColumnCount()];

			for (int i = 0; i < meta.getColumnCount(); i++)
				titles[i] = meta.getColumnName(i + 1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return titles;
	}

	public int getNumRecord() {
		int num = 0;
		String sql = "select count(*) from staff";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				num = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	public String[] getNames() {
		String[] names = null;
		String sql = "select * from staff";
		int i = 0;
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			names = new String[getNumRecord()];

			while (rs.next())
				names[i++] = rs.getString(2);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public int getIdByName(String name) {
		int id = 0;
		String sql = "select stid from staff where stname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				id = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public List<Integer> getIds() {
		List<Integer> list = new ArrayList<Integer>();
		String sql = "select stid from staff";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getInt(1));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public String getCurDate() {
		String date = "";
		String sql = "select curdate()";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				date = rs.getString(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static void main(String[] args) {
		Staff s = new Staff();
		System.out.println(s.getCurDate().split("-")[3]);
	}

	/*
	 * public boolean isExist(String name) { String sql =
	 * "select  from department where dname = ?"; try { cn =
	 * Database.getConnetion(); PreparedStatement ps = cn.prepareStatement(sql);
	 * ps.setString(1, name); ResultSet rs = ps.executeQuery(); if (rs.next()) {
	 * rs.close(); ps.close(); cn.close(); return true; } rs.close(); ps.close();
	 * cn.close(); } catch (SQLException e) { e.printStackTrace(); } return false; }
	 */
}
