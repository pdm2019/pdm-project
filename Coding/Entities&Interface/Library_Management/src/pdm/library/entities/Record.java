package pdm.library.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Record {
	private Connection cn;
	private int id;
	private int sid;
	private int stid;
	private String borrowdate;
	private String duedate;

	public Record(int id, int sid, int stid, String borrowdate, String duedate) {
		super();
		this.id = id;
		this.sid = sid;
		this.stid = stid;
		this.borrowdate = borrowdate;
		this.duedate = duedate;
	}

	public Record() {
		super();
	}

	public Connection getCn() {
		return cn;
	}

	public void setCn(Connection cn) {
		this.cn = cn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public int getStid() {
		return stid;
	}

	public void setStid(int stid) {
		this.stid = stid;
	}

	public String getBorrowdate() {
		return borrowdate;
	}

	public void setBorrowdate(String borrowdate) {
		this.borrowdate = borrowdate;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public List<Record> getAll() {
		List<Record> list = new ArrayList<Record>();
		String sql = "select * from record";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Record(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5)));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public String[] getTitles() {
		String[] titles = null;
		String sql = "select * from record";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			titles = new String[meta.getColumnCount()];

			for (int i = 0; i < meta.getColumnCount(); i++)
				titles[i] = meta.getColumnName(i + 1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return titles;
	}

	public int getNumRecord() {
		int num = 0;
		String sql = "select count(*) from record";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				num = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	public String[] getNames() {
		String[] names = null;
		String sql = "select * from record";
		int i = 0;
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			names = new String[getNumRecord()];

			while (rs.next())
				names[i++] = rs.getString(2);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	/*
	 * public int getIdByName(String name) { int id = 0; String sql =
	 * "select reid from record where dname = ?"; try { cn =
	 * Database.getConnetion(); PreparedStatement ps = cn.prepareStatement(sql);
	 * ps.setString(1, name); ResultSet rs = ps.executeQuery(); if (rs.next()) id =
	 * rs.getInt(1); rs.close(); ps.close(); cn.close(); } catch (SQLException e) {
	 * e.printStackTrace(); } return id; }
	 */

	public boolean isExist(String name) {
		String sql = "select dname from department where dname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.close();
				ps.close();
				cn.close();
				return true;
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * public int getLastRecord() {
	 * 
	 * int reid = 0; String sql = "select dname from department where dname = ?";
	 * try { cn = Database.getConnetion(); PreparedStatement ps =
	 * cn.prepareStatement(sql); ps.setString(1, name); ResultSet rs =
	 * ps.executeQuery(); if (rs.next()) { rs.close(); ps.close(); cn.close();
	 * return true; } rs.close(); ps.close(); cn.close(); } catch (SQLException e) {
	 * e.printStackTrace(); }
	 * 
	 * return reid; }
	 */

}
