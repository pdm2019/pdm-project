package pdm.library.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Author {
	private Connection cn;
	private int id;
	private String name;
	private String date;

	public Author(int id, String name, String date) {
		this.id = id;
		this.name = name;
		this.date = date;
	}

	public Author() {
		/* cn = Database.getConnetion(); */
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String[] getTitles() {
		String[] titles = null;
		String sql = "select * from author";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			titles = new String[meta.getColumnCount()];

			for (int i = 0; i < meta.getColumnCount(); i++)
				titles[i] = meta.getColumnName(i + 1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return titles;
	}

	public int getNumRecord() {
		int num = 0;
		String sql = "select count(*) from author";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				num = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	public String[] getNames() {
		String[] names = null;
		String sql = "select * from author";
		int i = 0;
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			names = new String[getNumRecord()];

			while (rs.next())
				names[i++] = rs.getString(2);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}
	
	public int getIdByName(String name) {
		int id = 0;
		String sql = "select aid from author where aname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				id = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public List<Author> getAll() {
		List<Author> list = new ArrayList<Author>();
		String sql = "select * from author";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Author(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
}
