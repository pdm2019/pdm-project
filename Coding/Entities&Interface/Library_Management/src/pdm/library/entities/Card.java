package pdm.library.entities;

import java.sql.Connection;

public class Card {
	private Connection cn;
	private int reid;
	private int bid;

	public Card(int reid, int bid) {
		super();
		this.reid = reid;
		this.bid = bid;
	}

	public Card() {
		super();
	}

	public Connection getCn() {
		return cn;
	}

	public void setCn(Connection cn) {
		this.cn = cn;
	}

	public int getReid() {
		return reid;
	}

	public void setReid(int reid) {
		this.reid = reid;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

}
