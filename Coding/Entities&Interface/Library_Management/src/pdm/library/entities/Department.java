package pdm.library.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Department {
	private Connection cn;
	private int id;
	private String name;

	public Department(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Department() {
		super();
	}

	public Connection getCn() {
		return cn;
	}

	public void setCn(Connection cn) {
		this.cn = cn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Department> getAll() {
		List<Department> list = new ArrayList<Department>();
		String sql = "select * from department";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Department(rs.getInt(1), rs.getString(2)));
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public String[] getTitles() {
		String[] titles = null;
		String sql = "select * from department";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			titles = new String[meta.getColumnCount()];

			for (int i = 0; i < meta.getColumnCount(); i++)
				titles[i] = meta.getColumnName(i + 1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return titles;
	}

	public int getNumRecord() {
		int num = 0;
		String sql = "select count(*) from department";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				num = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	public String[] getNames() {
		String[] names = null;
		String sql = "select * from department";
		int i = 0;
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			names = new String[getNumRecord()];

			while (rs.next())
				names[i++] = rs.getString(2);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}
	
	public int getIdByName(String name) {
		int id = 0;
		String sql = "select did from department where dname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				id = rs.getInt(1);
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public boolean isExist(String name) {
		String sql = "select dname from department where dname = ?";
		try {
			cn = Database.getConnetion();
			PreparedStatement ps = cn.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.close();
				ps.close();
				cn.close();
				return true;
			}
			rs.close();
			ps.close();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	

}
